###############################################################################
# Author: N. Fernandez
# Affiliation: IAC
# Aim: Snakemake meta-barcoding workflow based on qiime2-2018-11
# Date: 2017.11.02
# Run: snakemake -j [core] --use-conda
# Latest modification: 2018.11.16
# Todo:

###############################################################################
configfile: "config.yaml"

###############################################################################
# Wildcards
PROJECT = config["datasets"]["project"]
#PRIMERS = config["datasets"]["primers"]

# Environment
QIIME2 = config["conda"]["qiime2"] # qiime2

# Params
THREADS = config["cluster"]["threads"] # general

###############################################################################
rule all:
    input:
        out = expand("done/17_py_funguild_{project}",
                     project = PROJECT)

###############################################################################
rule py_funguild:
    # Aim: parse fungal community by ecological guild with FUNGuild tool
    # Use: snakefile 17_py_funguild
    message:
        ("Running 17_py_funguild "
         +"on {wildcards.project} project "
         +"with {params.threads} threads")
    params:
        threads = THREADS
    input:
        "done/16_biom_convert_{project}"
    output:
        touch("done/17_py_funguild_{project}")
    shell:
        "snakemake "
        #"--dryrun "
        # -n: Do not execute anything, and display what would be done.
        #"--reason "
        # -r: Print the reason for each executed rule.
        #"--quiet "
        # -q: Do not output any progress or rule information.
        "--cores {params.threads} "
        # -j: Use at most N cores in parallel (default: 1).
        "--snakefile snk/17_py_funguild "
        # -s: The workflow definition in a snakefile.
        "--configfile config.yaml"
        # -C: Set or overwrite values in the workflow config object.
        
###############################################################################
rule biom_convert:
    # Aim: convert BIOM format to table format
    # Use: snakefile 16_biom_convert
    message:
        ("Running 16_biom_convert "
         +"on {wildcards.project} project "
         +"with {params.threads} threads")
    conda:
        QIIME2
    params:
        threads = THREADS
    input:
        "done/15_qiime2_export_{project}"
    output:
        touch("done/16_biom_convert_{project}")
    shell:
        "snakemake "
        #"--dryrun "
        # -n: Do not execute anything, and display what would be done.
        #"--reason "
        # -r: Print the reason for each executed rule.
        "--quiet "
        # -q: Do not output any progress or rule information.
        "--cores {params.threads} "
        # -j: Use at most N cores in parallel (default: 1).
        "--snakefile snk/16_biom_convert "
        # -s: The workflow definition in a snakefile.
        "--configfile config.yaml"
        # -C: Set or overwrite values in the workflow config object.

###############################################################################
rule qiime2_export:
    # Aim: export data from QIIME2 artifact.qza and visualization.qzv files
    # Use: snakefile 15_qiime2_export
    message:
        ("Running 15_qiime2_export "
         +"on {wildcards.project} project "
         +"with {params.threads} threads")
    conda:
        QIIME2
    params:
        threads = THREADS
    input:
        "done/14_qiime2_gneiss_{project}"
    output:
        touch("done/15_qiime2_export_{project}")
    shell:
        "snakemake "
        #"--dryrun "
        # -n: Do not execute anything, and display what would be done.
        #"--reason "
        # -r: Print the reason for each executed rule.
        #"--quiet "
        # -q: Do not output any progress or rule information.
        "--cores {params.threads} "
        # -j: Use at most N cores in parallel (default: 1).
        "--snakefile snk/15_qiime2_export "
        # -s: The workflow definition in a snakefile.
        "--configfile config.yaml"
        # -C: Set or overwrite values in the workflow config object.

###############################################################################
rule qiime2_gneiss:
    # Aim: identify features that are differentially abundant across groups
    # Use: snakefile 14_qiime2_gneiss
    message:
        ("Running 14_qiime2_gneiss "
         +"on {wildcards.project} project "
         +"with {params.threads} threads")
    conda:
        QIIME2
    params:
        threads = THREADS
    input:
        "done/13_qiime2_ancom_{project}"
    output:
        touch("done/14_qiime2_gneiss_{project}")
    shell:
        "snakemake "
        #"--dryrun "
        # -n: Do not execute anything, and display what would be done.
        #"--reason "
        # -r: Print the reason for each executed rule.
        #"--quiet "
        # -q: Do not output any progress or rule information.
        "--cores {params.threads} "
        # -j: Use at most N cores in parallel (default: 1).
        "--snakefile snk/14_qiime2_gneiss "
        # -s: The workflow definition in a snakefile.
        "--configfile config.yaml"
        # -C: Set or overwrite values in the workflow config object.

###############################################################################
rule qiime2_ancom:
    # Aim: identify features that are differentially abundant across groups
    # Use: snakefile 13_qimme2_ancom
    message:
        ("Running 13_qimme2_ancom "
         +"on {wildcards.project} project "
         +"with {params.threads} threads")
    conda:
        QIIME2
    params:
        threads = THREADS
    input:
        "done/12_qiime2_taxonomy_{project}"
    output:
        touch("done/13_qiime2_ancom_{project}")
    shell:
        "snakemake "
        #"--dryrun "
        # -n: Do not execute anything, and display what would be done.
        #"--reason "
        # -r: Print the reason for each executed rule.
        #"--quiet "
        # -q: Do not output any progress or rule information.
        "--cores {params.threads} "
        # -j: Use at most N cores in parallel (default: 1).
        "--snakefile snk/13_qiime2_ancom "
        # -s: The workflow definition in a snakefile.
        "--configfile config.yaml"
        # -C: Set or overwrite values in the workflow config object.

###############################################################################
rule qiime2_taxonomy:
    # Aim: classify reads by taxon using a fitted classifier
    # Use: snakefile 12_qiime2_taxonomy
    message:
        ("Running 12_qiime2_taxonomy "
         +"on {wildcards.project} project "
         +"with {params.threads} threads")
    conda:
        QIIME2
    params:
        threads = THREADS
    input:
        "done/11_qiime2_corebiom_{project}"
    output:
        touch("done/12_qiime2_taxonomy_{project}")
    shell:
        "snakemake "
        #"--dryrun "
        # -n: Do not execute anything, and display what would be done.
        #"--reason "
        # -r: Print the reason for each executed rule.
        #"--quiet "
        # -q: Do not output any progress or rule information.
        "--cores {params.threads} "
        # -j: Use at most N cores in parallel (default: 1).
        "--snakefile snk/12_qiime2_taxonomy "
        # -s: The workflow definition in a snakefile.
        "--configfile config.yaml"
        # -C: Set or overwrite values in the workflow config object.

###############################################################################
rule qiime2_corebiom:
    # Aim: identify "core" features observed in a fraction of the samples
    # Use: snakefile 11_qiime2_corebiom
    message:
        ("Running 11_qiime2_corebiom "
         +"on {wildcards.project} project "
         +"with {params.threads} threads")
    conda:
        QIIME2
    params:
        threads = THREADS
    input:
        "done/10_qiime2_significance_{project}"
    output:
        touch("done/11_qiime2_corebiom_{project}")
    shell:
        "snakemake "
        #"--dryrun "
        # -n: Do not execute anything, and display what would be done.
        #"--reason "
        # -r: Print the reason for each executed rule.
        #"--quiet "
        # -q: Do not output any progress or rule information.
        "--cores {params.threads} "
        # -j: Use at most N cores in parallel (default: 1).
        "--snakefile snk/11_qiime2_corebiom "
        # -s: The workflow definition in a snakefile.
        "--configfile config.yaml"
        # -C: Set or overwrite values in the workflow config object.

###############################################################################
rule qiime2_significance:
    # Aim: statistically compare groups of alpha/beta diversity values
    # Use: snakefile 10_qiime2_significance
    message:
        ("Running 10_qiime2_significance "
         +"on {wildcards.project} project "
         +"with {params.threads} threads")
    conda:
        QIIME2
    params:
        threads = THREADS
    input:
        "done/09_qiime2_more_{project}"
    output:
        touch("done/10_qiime2_significance_{project}")
    shell:
        "snakemake "
        #"--dryrun "
        # -n: Do not execute anything, and display what would be done.
        #"--reason "
        # -r: Print the reason for each executed rule.
        #"--quiet "
        # -q: Do not output any progress or rule information.
        "--cores {params.threads} "
        # -j: Use at most N cores in parallel (default: 1).
        "--snakefile snk/10_qiime2_significance "
        # -s: The workflow definition in a snakefile.
        "--configfile config.yaml"
        # -C: Set or overwrite values in the workflow config object.
        
###############################################################################
rule qiime2_more:
    # Aim: perform more diversity metrics
    # Use: snakefile 09_qiime2_more
    message:
        ("Running 09_qiime2_more "
         +"on {wildcards.project} project "
         +"with {params.threads} threads")
    conda:
        QIIME2
    params:
        threads = THREADS
    input:
        "done/08_qiime2_metrics_{project}"
    output:
        touch("done/09_qiime2_more_{project}")
    shell:
        "snakemake "
        #"--dryrun "
        # -n: Do not execute anything, and display what would be done.
        #"--reason "
        # -r: Print the reason for each executed rule.
        #"--quiet "
        # -q: Do not output any progress or rule information.
        "--cores {params.threads} "
        # -j: Use at most N cores in parallel (default: 1).
        "--snakefile snk/09_qiime2_more "
        # -s: The workflow definition in a snakefile.
        "--configfile config.yaml"
        # -C: Set or overwrite values in the workflow config object.

###############################################################################
rule qiime2_metrics:
    # Aim: perform rarefaction table and diversity metrics
    # Use: snakefile 08_qiime2_metrics
    message:
        ("Running 08_qiime2_metrics "
         +"on {wildcards.project} project "
         +"with {params.threads} threads")
    conda:
        QIIME2
    params:
        threads = THREADS
    input:
        "done/07_qiime2_rarefaction_{project}"
    output:
        touch("done/08_qiime2_metrics_{project}")
    shell:
        "snakemake "
        #"--dryrun "
        # -n: Do not execute anything, and display what would be done.
        #"--reason "
        # -r: Print the reason for each executed rule.
        #"--quiet "
        # -q: Do not output any progress or rule information.
        "--cores {params.threads} "
        # -j: Use at most N cores in parallel (default: 1).
        "--snakefile snk/08_qiime2_metrics "
        # -s: The workflow definition in a snakefile.
        "--configfile config.yaml"
        # -C: Set or overwrite values in the workflow config object.

###############################################################################
rule qiime2_rarefaction:
    # Aim: 
    # Use: snakefile 07_qiime2_rarefaction
    message:
        ("Running 07_qiime2_rarefaction "
         +"on {wildcards.project} project "
         +"with {params.threads} threads")
    conda:
        QIIME2
    params:
        threads = THREADS
    input:
        "done/06_qiime2_tree_{project}"
    output:
        touch("done/07_qiime2_rarefaction_{project}")
    shell:
        "snakemake "
        #"--dryrun "
        # -n: Do not execute anything, and display what would be done.
        #"--reason "
        # -r: Print the reason for each executed rule.
        #"--quiet "
        # -q: Do not output any progress or rule information.
        "--cores {params.threads} "
        # -j: Use at most N cores in parallel (default: 1).
        "--snakefile snk/07_qiime2_rarefaction "
        # -s: The workflow definition in a snakefile.
        "--configfile config.yaml"
        # -C: Set or overwrite values in the workflow config object.

###############################################################################
rule qiime2_tree:
    # Aim: construct a rooted phylogenetic tree
    # Use: snakefile 06_qiime2_tree
    message:
        ("Running 06_qiime2_tree "
         +"on {wildcards.project} project "
         +"with {params.threads} threads")
    conda:
        QIIME2
    params:
        threads = THREADS
    input:
        "done/05_qiime2_denoise_{project}"
    output:
        touch("done/06_qiime2_tree_{project}")
    shell:
        "snakemake "
        #"--dryrun "
        # -n: Do not execute anything, and display what would be done.
        #"--reason "
        # -r: Print the reason for each executed rule.
        #"--quiet "
        # -q: Do not output any progress or rule information.
        "--cores {params.threads} "
        # -j: Use at most N cores in parallel (default: 1).
        "--snakefile snk/06_qiime2_tree "
        # -s: The workflow definition in a snakefile.
        "--configfile config.yaml"
        # -C: Set or overwrite values in the workflow config object.

###############################################################################
rule qiime2_denoise:
    # Aim: denoises sequences and call ASVs with DADA2 plugin
    # Use: snakefile 05_qiime2_denoise
    message:
        ("Running 05_qiime2_denoise "
         +"on {wildcards.project} project "
         +"with {params.threads} threads")
    conda:
        QIIME2
    params:
        threads = THREADS
    input:
        "done/04_qiime2_import_{project}"
    output:
        touch("done/05_qiime2_denoise_{project}")
    shell:
        "snakemake "
        #"--dryrun "
        # -n: Do not execute anything, and display what would be done.
        #"--reason "
        # -r: Print the reason for each executed rule.
        #"--quiet "
        # -q: Do not output any progress or rule information.
        "--cores {params.threads} "
        # -j: Use at most N cores in parallel (default: 1).
        "--snakefile snk/05_qiime2_denoise "
        # -s: The workflow definition in a snakefile.
        "--configfile config.yaml"
        # -C: Set or overwrite values in the workflow config object.

###############################################################################
rule qiime2_import:
    # Aim: import project data and create new qiime2 artifact
    # Use: snakefile 04_qiime2_import
    message:
        ("Running 04_qiime2_import "
         +"on {wildcards.project} project "
         +"with {params.threads} threads")
    conda:
        QIIME2
    params:
        threads = THREADS
    input:
        "done/03_quality_clean_{project}"
    output:
        touch("done/04_qiime2_import_{project}")
    shell:
        "snakemake "
        #"--dryrun "
        # -n: Do not execute anything, and display what would be done.
        #"--reason "
        # -r: Print the reason for each executed rule.
        #"--quiet "
        # -q: Do not output any progress or rule information.
        "--cores {params.threads} "
        # -j: Use at most N cores in parallel (default: 1).
        "--snakefile snk/04_qiime2_import "
        # -s: The workflow definition in a snakefile.
        "--configfile config.yaml"
        # -C: Set or overwrite values in the workflow config object.

###############################################################################
rule quality_clean:
    # Aim: produces a reads quality control report
    # Use: snakefile 03_quality_clean
    message:
        ("Running 03_quality_clean "
         +"on {wildcards.project} project "
         +"with {params.threads} threads")
    params:
        threads = THREADS
    input:
        "done/02_clean_reads_{project}"
    output:
        touch("done/03_quality_clean_{project}")
    shell:
        "snakemake "
        #"--dryrun "
        # -n: Do not execute anything, and display what would be done.
        #"--reason "
        # -r: Print the reason for each executed rule.
        #"--quiet "
        # -q: Do not output any progress or rule information.
        "--cores {params.threads} "
        # -j: Use at most N cores in parallel (default: 1).
        "--snakefile snk/03_quality_clean "
        # -s: The workflow definition in a snakefile.
        "--configfile config.yaml"
        # -C: Set or overwrite values in the workflow config object.

###############################################################################
rule clean_reads:
    # Aim: clean reads
    # Use: snakefile 02_clean_reads
    message:
        ("Running 02_clean_reads "
         +"on {wildcards.project} project "
         +"with {params.threads} threads")
    params:
        threads = THREADS
    input:
        "done/01_quality_raw_{project}"
    output:
        touch("done/02_clean_reads_{project}")
    shell:
        "snakemake "
        #"--dryrun "
        # -n: Do not execute anything, and display what would be done.
        #"--reason "
        # -r: Print the reason for each executed rule.
        #"--quiet "
        # -q: Do not output any progress or rule information.
        "--cores {params.threads} "
        # -j: Use at most N cores in parallel (default: 1).
        "--snakefile snk/02_clean_reads "
        # -s: The workflow definition in a snakefile.
        "--configfile config.yaml"
        # -C: Set or overwrite values in the workflow config object.

###############################################################################
rule quality_raw:
    # Aim: produces a raw reads quality control report
    # Use: snakefile 01_quality_raw
    message:
        ("Running 01_quality_raw "
         +"on {wildcards.project} project "
         +"with {params.threads} threads")
    params:
        threads = THREADS
    input:
        "done/00_gunzip_fastq_{project}"
    output:
        touch("done/01_quality_raw_{project}")
    shell:
        "snakemake "
        #"--dryrun "
        # -n: Do not execute anything, and display what would be done.
        #"--reason "
        # -r: Print the reason for each executed rule.
        #"--quiet "
        # -q: Do not output any progress or rule information.
        "--cores {params.threads} "
        # -j: Use at most N cores in parallel (default: 1).
        "--snakefile snk/01_quality_raw "
        # -s: The workflow definition in a snakefile.
        "--configfile config.yaml"
        # -C: Set or overwrite values in the workflow config object.

###############################################################################
rule gunzip_fastq:
    # Aim: gunzip fastq.gz files
    # Use: snakefile 00_gunzip_fastq
    message:
        ("Running 00_gunzip_fastq "
         +"on {wildcards.project} project "
         +"with {params.threads} threads")
    params:
        threads = THREADS
    input:
        "inp/reads/{project}/"
    output:
        touch("done/00_gunzip_fastq_{project}")
    shell:
        "snakemake "
        #"--dryrun "
        # -n: Do not execute anything, and display what would be done.
        #"--reason "
        # -r: Print the reason for each executed rule.
        #"--quiet "
        # -q: Do not output any progress or rule information.
        "--cores {params.threads} "
        # -j: Use at most N cores in parallel (default: 1).
        "--snakefile snk/00_gunzip_fastq "
        # -s: The workflow definition in a snakefile.
        "--configfile config.yaml"
        # -C: Set or overwrite values in the workflow config object.

###############################################################################
