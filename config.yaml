---
## CLUSTER --------------------------------------------------------------------
cluster:
  threads: '6'

## ENVIRONNEMENTS -------------------------------------------------------------
conda:
  quality: 'env/quality.yaml'
  qiime2: 'env/qiime2-2018.08.yaml'
  r :
    #- 'env/R-3.2.2.yaml'
    #- 'env/R-3.4.3.yaml'
    - 'env/R-3.5.1.yaml'
 
## DATASETS -------------------------------------------------------------------
datasets:
  metadata: 'inp/qiime2/metadata.tsv' # path to my sample metadata file
  project: 'bioindic'                 # my project
  primers:                            # my primers
    - 'its2'                           # set1, e.g. fungi
    - 'v4'                             # set2, e.g. bacteria
    #- 'nSSU'                          # set3, e.g. nematoda
  filters:
    - ''
    - 'Neg'
    - 'Con'
#  
#    base:             # Summarize_base* | Tree | Metrics
#      #- 'FilNeg'
#      - 'FilCon'
#    rare:             # More | PCoA | Significance | R_Stats
##      #- 'RarFilNeg'
#      - 'RarFilCon'
#    both:             # Rules Corebiom | Taxonomy | Export | Biom | FunGUILD | inext | Jaunatre
#      #- 'FilNeg'
#      - 'FilCon'
#      #- 'RarFilNeg'
#      - 'RarFilCon'
#

## CUTADAPT -------------------------------------------------------------------
cutadapt:
  truseq:      'AGATCGGAAGAGC'                     # Illumina TruSeq / ScriptSeq based kits libraries
  nextera:     'CTGTCTCTTATACACATC'                # Illumina Nextera / TruSight based kits libraries
  small:       'TGGAATTCTCGGGTGCCAAGG'             # Illumina Small based kits libraries
  TruSeqR1:    'AGATCGGAAGAGCACACGTCTGAACTCCAGTCA' # TruSeq-LT and TruSeq-HT based kits R1
  TruSeqR2:    'AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT' # TruSeq-LT and TruSeq-HT based kits R2
  ScriptSeqR1: 'AGATCGGAAGAGCACACGTCTGAAC'         # ScriptSeq and TruSeq DNA Methylation based kits R1
  ScriptSeqR2: 'AGATCGGAAGAGCGTCGTGTAGGGA'         # ScriptSeq and TruSeq DNA Methylation based kits R2
  TruSeqRibo:  'AGATCGGAAGAGCACACGTCT'             # TruSeq Ribo Profile based kits

## SICKLE ---------------------------------------------------------------------

sickle:
  commande: # choose one
    #- 'se'  # if single-end
    - 'pe'   # if paired-end
  encoding:       # choose one
    - 'sanger'     # if sanger   (CASAVA >= 1.8)
    #- 'illumina'  # if illumina (CASAVA 1.3 to 1.7)
    #- 'solexa'    # if solexa   (CASAVA < 1.3)
  quality: '30'   # phred score limit
  length: '150'   # read length limit, after trim

## QIIME2_IMPORT_DATA ---------------------------------------------------------------------------------

import:
  data_type:
    - ''           # if single-end sequences (or joined sequences with qiime 2017.10 and before)
    #- 'PairedEnd' # if paired-end sequences
    #- 'Joined'    # if joined sequences (only with qiime 2017.12 and later, and for visualization, not yet for dada2!)
  format:
    - 'SingleEnd'  # if single-end
    #- 'PairedEnd' # if paired-end
  encoding:
    - '33'  # if sanger or illumina (>=1.8)
    #- '64' # if solexa or illumina (<=1.8)

## QIIME2_DADA2_DENOIZING ----------------------------------------------------------------------------

dada2:
  chimera:
    #- 'none'       # No chimera removal is performed
    #- 'pooled'     # All reads are pooled prior to chimera detection
    - 'consensus'   # Chimeras are detected in samples individually and,
                    # sequences found chimeric in a sufficient fraction of samples are removed
  trunc_qual: '0' # Reads are truncated at the first instance of a quality score less than or equal to this value.
  trunc_len: '0'  # Reads that are shorter than this value will be discarded.
                   # After this parameter is applied there must still be at least a 20 nucleotide overlap.
                   # If 0 is provided, no truncation or length filtering will be performed.

## FILTERS_NEGCTRL_&_CONTINGENCY ---------------------------------------------------------------------

filter:
  negctrl:
    methode:
      #- 'blast'
      #- 'blastn-short'
      - 'vsearch'
    identity: '1.00'  # percentage identity
    aligned: '1.00'   # percentage aligned
  contingency:
    min_obs: 2  # Remove features that are present in only a single sample !
    min_freq: 0 # Remove features with a total abundance (summed across all samples) of less than 0 !

## METRICS_DEPTH ----------------------------------------------------------------------------------

depth:           # The even sampling depth (i.e. rarefaction)
  bioindic:
    its2: '3215' # Loose 7 subplots, retain 116 on 123 (94.31%) (-1 contingence)
    v4: '4171'   # Loose 6 subplots, retain 117 on 123 (95.12%) (-1 contingence)
    nSSU: ''
  other:
    its2: ''
    v4: ''
    nSSU: ''

## CORE_BIOM ---------------------------------------------------------------------------------------

corebiom:
  min_fraction: 0.1
  max_fraction: 1.0
  steps: 10
  formation:
    - 'MAO'
    - 'MD'
    - 'MPGd'
    - 'MP'
    - 'ZR'

## INDICES_DIVERSITY ---------------------------------------------------------------------------------

diversity:
  alpha:
    more:                      # At least one
      #- 'observed_asv'        # Number of distinct features: Calculates number of distinct ESVs/ASVs/OTUs.
      #- 'shannon'              # Shannon's index: Calculates richness and diversity using a natural logarithm.
                                # Accounts for both abundance and evenness of the taxa present.
      #- 'faith_pd'             # Faith's phylogenetic diversity: Measures of biodiversity (Sum of length of phylogenetic branches).
      - 'simpson'              # Simpson's index: Measures the relative abundance of the different species making up the sample richness.
      - 'simpson_e'            # Simpson evenness measure E: Diversity that account for the number of organisms and number of species.
      - 'fisher_alpha'         # Fisher's index: Relationship between the number of species and the abundance of each species.
      - 'pielou_e'             # Pielou's evenness: Measure of relative evenness of species richness.
      - 'chao1'                # Chao1's index: Estimates diversity from abundant data and number of rare taxa missed from under sampling.
      - 'chao1_ci'             # Chao1 confidence interval: Confidence interval for richness estimator chao1.
      #- 'gini_index'           # Gini index: Measures species abundance. Assumes that the sampling is accurate,
                                 # and that additional data would fall on linear gradients between the values of the given data.
      #- 'ace'                  # Abundance-based Coverage Estimator (ACE) metric: Estimates species richness using a correction factor.
      #- 'berger_parker_d'      # Berger-Parker Dominance Index: Relative richness of the abundant species.
      #- 'brillouin_d'          # Brillouin's index: Measures the diversity of the species present.
      #- 'dominance'            # Dominance measure: How equally the taxa are presented.
      #- 'enspie'               # Effective Number of Species (ENS) / Probability of intra-or interspecific encounter (PIE) metric:
                                 # Shows how absolute amount of species, relative abundances of species and,
                                 # their intraspecific clustering affect differences in biodiversity among communities.
      #- 'esty_ci'              # Etsy confidence interval: Confidence interval for how many singletons in total individuals.
      #- 'goods_coverage'       # Good's coverage of counts: Estimates the percent of an entire species that is represented in a sample.
      #- 'heip_e'               # Heip's evenness measure: Removes dependency on species number.
      #- 'kempton_taylor_q'     # Kempton-Taylor Q index: Measured diversity based off the distributions of species.
      #- '                       # Makes abundance curve based off all species and IQR is used to measure diversity.
      #- 'lladser_pe'           # Lladser's point estimate: Estimates how much of the environment contains unsampled taxa.
      #- '                       # Best estimate on a complete sample.
      #- 'lladser_ci'           # Lladser's confidence interval: Single confidence interval of the conditional uncovered probability.
      #- 'margalef'             # Margalef's richness index: Measures species richness in a given area or community.
      #- 'mcintosh_d'           # Mcintosh dominance index D: Affected by the variation in dominant taxa
                                 # and less affected by the variation in less abundant or rare taxa.
      #- 'mcintosh_e'           # Mcintosh evenness index E: How evenly abundant taxa are.
      #- 'menhinick'            # Menhinick's richness index: The ratio of the number of taxa to the square root of the sample size.
      #- 'michaelis_menten_fit' # Michaelis-Menten fit to rarefaction curve of observed OTUs: Estimated richness of species pools.
      #- 'doubles'              # Number of double occurrences: ESVs/ASVs/OTUs that only occur twice.
      #- 'singles'              # Number of songlr occurrences: ESVs/ASVs/OTUs that appear only once in a given sample.
      #- 'osd'                  # Number of observed features: Including singles and doubles.
      #- 'robbins'              # Robbins' estimator: Probability of unobserved outcomes.
      #- 'strong'               # Strong's dominance index (Dw): Measures species abundance unevenness.
    significance:              # At least one
      - 'observed_asv'        # Number of distinct features: Calculates number of distinct ESVs/ASVs/OTUs.
      - 'shannon'              # Shannon's index: Calculates richness and diversity using a natural logarithm.
                                 # Accounts for both abundance and evenness of the taxa present.
      #- 'faith_pd'             # Faith's phylogenetic diversity: Measures of biodiversity (Sum of length of phylogenetic branches).
      - 'simpson'              # Simpson's index: Measures the relative abundance of the different species making up the sample richness.
      #- 'simpson_e'            # Simpson evenness measure E: Diversity that account for the number of organisms and number of species.
      #- 'fisher_alpha'         # Fisher's index: Relationship between the number of species and the abundance of each species.
      #- 'pielou_e'             # Pielou's evenness: Measure of relative evenness of species richness.
      #- 'chao1'                # Chao1's index: Estimates diversity from abundant data and number of rare taxa missed from under sampling.
      #- 'chao1_ci'             # Chao1 confidence interval: Confidence interval for richness estimator chao1.
      #- 'gini_index'           # Gini index: Measures species abundance. Assumes that the sampling is accurate,
                                 # and that additional data would fall on linear gradients between the values of the given data.
      #- 'ace'                  # Abundance-based Coverage Estimator (ACE) metric: Estimates species richness using a correction factor.
      #- 'berger_parker_d'      # Berger-Parker Dominance Index: Relative richness of the abundant species.
      #- 'brillouin_d'          # Brillouin's index: Measures the diversity of the species present.
      #- 'dominance'            # Dominance measure: How equally the taxa are presented.
      #- 'enspie'               # Effective Number of Species (ENS) / Probability of intra-or interspecific encounter (PIE) metric:
                                 # Shows how absolute amount of species, relative abundances of species and,
                                 # their intraspecific clustering affect differences in biodiversity among communities.
      #- 'esty_ci'              # Etsy confidence interval: Confidence interval for how many singletons in total individuals.
      #- 'goods_coverage'       # Good's coverage of counts: Estimates the percent of an entire species that is represented in a sample.
      #- 'heip_e'               # Heip's evenness measure: Removes dependency on species number.
      #- 'kempton_taylor_q'     # Kempton-Taylor Q index: Measured diversity based off the distributions of species.
      #- '                       # Makes abundance curve based off all species and IQR is used to measure diversity.
      #- 'lladser_pe'           # Lladser's point estimate: Estimates how much of the environment contains unsampled taxa.
      #- '                       # Best estimate on a complete sample.
      #- 'lladser_ci'           # Lladser's confidence interval: Single confidence interval of the conditional uncovered probability.
      #- 'margalef'             # Margalef's richness index: Measures species richness in a given area or community.
      #- 'mcintosh_d'           # Mcintosh dominance index D: Affected by the variation in dominant taxa
                                 # and less affected by the variation in less abundant or rare taxa.
      #- 'mcintosh_e'           # Mcintosh evenness index E: How evenly abundant taxa are.
      #- 'menhinick'            # Menhinick's richness index: The ratio of the number of taxa to the square root of the sample size.
      #- 'michaelis_menten_fit' # Michaelis-Menten fit to rarefaction curve of observed OTUs: Estimated richness of species pools.
      #- 'doubles'              # Number of double occurrences: ESVs/ASVs/OTUs that only occur twice.
      #- 'singles'              # Number of songlr occurrences: ESVs/ASVs/OTUs that appear only once in a given sample.
      #- 'osd'                  # Number of observed features: Including singles and doubles.
      #- 'robbins'              # Robbins' estimator: Probability of unobserved outcomes.
      #- 'strong'               # Strong's dominance index (Dw): Measures species abundance unevenness.
    rarefaction:
      vector: # At least one
        - 'observed_asv'
        #- 'pielou_e'
        #- 'shannon'
        #- 'simpson'
        #- 'simpson_e'
        #- 'faith_pd'
        #- 'fisher_alpha'
        #- 'chao1'
        #- 'menhinick'
        #- 'berger_parker_d'
        #- 'mcintosh_d'
        #- 'dominance'
        #- 'lladser_pe'
        #- 'enspie'
        #- 'robbins'
        #- 'brillouin_d'
        #- 'mcintosh_e'
        #- 'ace'
        #- 'michaelis_menten_fit'
        #- 'margalef'
        #- 'goods_coverage'
        #- 'heip_e'
        #- 'singles'
        #- 'doubles'
        #- 'gini_index'
      min_depth: '1'    # Minimum rarefaction depth ; default: 1
      max_depth: '4000' # Maximum rarefaction depth. Must be greater than min_depth
      steps: '10 '      # Rarefaction steps to include between min and max depth ; default: 10
      iterations: '10'  # The number of rarefied feature tables to compute at each step ; default: 10
    correlation: # The correlation test to be applied. [default: spearman].
      - 'spearman'
      #- 'pearson'
  beta:
    more:                             # At least one
      - 'jaccard'                     # Jaccard similarity index: Fraction of unique features, regardless of abundance.
      #- 'braycurtis'                  # Bray-Curtis dissimilarity: Fraction of overabundant counts.
      #- 'unweighted_unifrac'          # Unweighted unifrac: Measures the fraction of unique branch length.
      #- 'weighted_unifrac'            # Weighted unnormalized UniFrac: Takes into account abundance.
                                        # Doesn't correct for unequal sampling effort or different evolutionary rates between taxa.
      #- 'weighted_normalized_unifrac' # Weighted normalized UniFrac: Takes into account abundance.
                                        # Normalization adjusts for varying root-to-tip distances.
      #- 'generalized_unifrac'         # Generalized Unifrac: Detects a wider range of biological changes compared to others UniFrac.
      #- 'canberra'                    # Canberra distance: Overabundance on a feature by feature basis.
      #- 'chebyshev'                   # Chebyshev distance: Maximum distance between two samples.
      #- 'cityblock'                   # City-block distance: Similar to the Euclidean distance but,
                                        # the effect of a large difference in a single dimension is reduced.
      #- 'correlation'                 # Correlation coefficient: Measure of strength and direction of linear relationship between samples
      #- 'cosine'                      # Cosine Similarity: Ratio of the amount of common species in a sample to the mean of the two samples.
      #- 'dice'                        # Dice measures: Statistic used for comparing the similarity of two samples. Only counts true positives once.
      #- 'euclidean'                   # Euclidean distance: Species-by-species distance matrix.
      #- 'hamming'                     # Hamming distance: Minimum number of substitutions required to change one group to the other.
      #- 'kulsinski'                   # Kulczynski dissimilarity index: Describes the dissimilarity between two samples.
      #- 'mahalanobis'                 # Mahalanobis distance: How many standard deviations one sample is away from the mean.
                                        # Unitless and scale-invariant.
      #- 'matching'                    # Matching components: Compares indices under all possible situations.
      #- 'rogerstanimoto'              # Rogers-tanimoto distance: Allows the possibility of two samples,
                                        # which are quite different from each other, to both be similar to a third.
      #- 'russellrao'                  # Russel-Rao coefficient: Equal weight is given to matches and non-matches.
      #- 'sokalmichener'               # Sokal-Michener coefficient: Proportion of matches between samples.
      #- 'sokalsneath'                 # Sokal-Sneath Index: Measure of species turnover.
      #- 'seuclidean'                  # Species-by-species Euclidean: Standardized Euclidean distance between two groups.
      #- 'sqeuclidean'                 # Squared Euclidean: Place progressively greater weight on samples that are farther apart.
      #- 'weighted minkowski'          # Weighted Minkowski metric: Allows the use of the k-means-type paradigm to cluster large data sets.
      #- 'yule'                        # Yule index: Determined by the diversity of species and the proportions between the abundance of those species
    significance:                     # At least one
      - 'jaccard'                     # Jaccard similarity index: Fraction of unique features, regardless of abundance.
      - 'braycurtis'                  # Bray-Curtis dissimilarity: Fraction of overabundant counts.
      #- 'unweighted_unifrac'          # Unweighted unifrac: Measures the fraction of unique branch length.
      #- 'weighted_unifrac'            # Weighted unnormalized UniFrac: Takes into account abundance.
                                        # Doesn't correct for unequal sampling effort or different evolutionary rates between taxa.
      #- 'weighted_normalized_unifrac' # Weighted normalized UniFrac: Takes into account abundance.
                                        # Normalization adjusts for varying root-to-tip distances.
      #- 'generalized_unifrac'         # Generalized Unifrac: Detects a wider range of biological changes compared to others UniFrac.
      #- 'canberra'                    # Canberra distance: Overabundance on a feature by feature basis.
      #- 'chebyshev'                   # Chebyshev distance: Maximum distance between two samples.
      #- 'cityblock'                   # City-block distance: Similar to the Euclidean distance but,
                                        # the effect of a large difference in a single dimension is reduced.
      #- 'correlation'                 # Correlation coefficient: Measure of strength and direction of linear relationship between samples
      #- 'cosine'                      # Cosine Similarity: Ratio of the amount of common species in a sample to the mean of the two samples.
      #- 'dice'                        # Dice measures: Statistic used for comparing the similarity of two samples. Only counts true positives once.
      #- 'euclidean'                   # Euclidean distance: Species-by-species distance matrix.
      #- 'hamming'                     # Hamming distance: Minimum number of substitutions required to change one group to the other.
      #- 'kulsinski'                   # Kulczynski dissimilarity index: Describes the dissimilarity between two samples.
      #- 'mahalanobis'                 # Mahalanobis distance: How many standard deviations one sample is away from the mean.
                                        # Unitless and scale-invariant.
      #- 'matching'                    # Matching components: Compares indices under all possible situations.
      #- 'rogerstanimoto'              # Rogers-tanimoto distance: Allows the possibility of two samples,
                                        # which are quite different from each other, to both be similar to a third.
      #- 'russellrao'                  # Russel-Rao coefficient: Equal weight is given to matches and non-matches.
      #- 'sokalmichener'               # Sokal-Michener coefficient: Proportion of matches between samples.
      #- 'sokalsneath'                 # Sokal-Sneath Index: Measure of species turnover.
      #- 'seuclidean'                  # Species-by-species Euclidean: Standardized Euclidean distance between two groups.
      #- 'sqeuclidean'                 # Squared Euclidean: Place progressively greater weight on samples that are farther apart.
      #- 'weighted minkowski'          # Weighted Minkowski metric: Allows the use of the k-means-type paradigm to cluster large data sets.
      #- 'yule'                        # Yule index: Determined by the diversity of species and the proportions between the abundance of those species.
    rarefaction:
      matrix: # At least one
        - 'jaccard'
        - 'braycurtis'
        #- 'unweighted_unifrac'
        #- 'weighted_unifrac'
        #- 'sokalsneath'
        #- 'correlation'
        #- 'hamming'
        #- 'matching'
        #- 'euclidean'
        #- 'russellrao'
        #- 'canberra'
        #- 'cityblock'
        #- 'sokalmichener'
        #- 'kulsinski'
        #- 'mahalanobis'
        #- 'cosine'
        #- 'yule'
        #- 'dice'
        #- 'wminkowski'
        #- 'seuclidean'
        #- 'chebyshev'
        #- 'rogerstanimoto'
        #- 'sqeuclidean'
      depth: '4000' # The total frequency that each sample should be rarefied to prior to computing the diversity metric.
      iterations: '10'
      clustering:
        #- 'nj'   # neighbor joining
        - 'upgma' # UPGMA, an arbitrary rarefaction trial will be used for the tree, and the remaining trials
                   # are used to calculate the support of the internal nodes of that tree.
      correlation: # The Mantel correlation test to be applied when computing correlation between distance matrices.
        - 'spearman'
        #- 'pearson'
      color: # The matplotlib color scheme to generate the heatmap with. Choose only one scheme. [default: BrBG]
        - 'BrBG'
        #- 'PuOr_r'
        #- 'RdYlGn_r'
        #- 'RdYlGn'
        #- 'RdBu_r'
        #- 'RdBu'
        #- 'PRGn_r'
        #- 'PiYG_r'
        #- 'RdGy'
        #- 'RdYlBu'
        #- 'PiYG'
        #- 'PuOr'
        #- 'RdGy_r'
        #- 'PRGn'
        #- 'RdYlBu_r'
        #- 'BrBG'
        #- 'BrBG_r'

## SIGNIFICANCE & PERMANOVA (R/vegan/adonis) --------------------------------------------------------------------------

permanova:
  pairwise:
    - 'pairwise'
    #- 'no-pairwise'
  permutations: 9999    # Default = 999
  categories:  # Categories tested
    - 'Acronyme'
    #- 'Site'
    #- 'SurfaceWind'
    #- 'Soil'
    #- 'Age'
    #- 'Gymno'
    #- 'Compagny'
    #- '...'       # See others metadata categories
  methode:
    - 'permanova'
    #- 'anosim'

## TAXONOMY --------------------------------------------------------------

taxonomy:
  batch_size: 1000 # Number of reads to process in each batch. [default: 0]
  # If 0, this parameter is autoscaled to the number of query sequences / n_jobs.

  its2:
    ribo_database:
      #- 'UNITE-V7-2017.10.10-dynamic'   # @dynamic homology with UNITE database (8.756-RefS & 21.793-RepS)
      #- 'UNITE-V7-2017.12.01-dynamic'   # @dynamic homology with UNITE database (8.997-RefS & 49.052-RepS)
      #- 'UNITE-V7-S-2017.12.01-dynamic' # @dynamic homology with UNITE database (8.997-RefS & 49.052-RepS)
      - 'IAC-V1-2017.10.10-dynamic'      # @dynamic homology with UNITE database (8.756-RefS & 21.793-RepS) + IAC intern DB (311-RefS Ecto)
      #- 'IAC-V2-2017.12.01-dynamic'     # @dynamic homology with UNITE database (8.997-RefS & 21.699-RepS) + IAC intern DB (311-RefS Ecto)
      #- 'IAC-V2-S-2017.12.01-dynamic'   # @dynamic homology with UNITE database (8.997-RefS & 49.052-RepS) + IAC intern DB (311-RefS Ecto
    forward: 'GTGARTCATCGAATCTTTG'  # fITS7
    reverse: 'TCCTCCGCTTATTGATATGC' # rITS4
    lenght:
      - 314  # based on MultiQC report after joining (314,0968). 47%GC
      #- 249 # theory
  v4:
    ribo_database:
      - 'SILVA-V132-2018.04.10-99' # @99% homology with SILVA database
      #- 'GREENGENE...-99'         # @99% homology with GREENGENE database    
    forward: 'GTGCCAGCMGCCGCGGTAA'  # 515f
    reverse: 'GGACTACHVGGGTWTCTAAT' # 806r
    lenght:
      - 291  # based on MultiQC report after joining (290,5807). 54%GC
      #- 249 # theory
  nSSU:
    ribo_database:
      - 'my_favorite_nematoda_database' # @ 95% homology    
    forward: '' # 
    reverse: '' # 
    lenght:
      - 000  # based on MultiQC report after joining (00,0000). 00%GC
      #- 000 # theory

# UNITE-INFO.
# Following Koljalg et al. (2013), each terminal fungal taxon for which two or more ITS sequences
 # are available is referred to as a species hypothesis (SH). One sequence is chosen to represent each SH.
# These sequences are called:
 # - Representative Sequences (RepS), when these sequences chosen automatically by the computer
 # - Reference Sequences (RefS), when those choices are overridden / confirmed by users with expert knowledge of the taxon at hand.

# SILVA-INFO.
 # Total number of sequences (all domains) for each clustering identity:
  # 99% --> 412.168
  # 97% --> 194.822
  # 94% -->  94.835
  # 90% -->  40.215
  # 80% -->   5.539
  # - 'raw_taxonomy'                  # Sequence IDs followed by the raw taxonomy strings directly pulled from the SILVA NR fasta file.
  # - 'taxonomy_7_levels'             # Raw taxa, forced into exactly 7 levels as described in the Silva_132_notes.
  # - 'taxonomy_all_levels'           # Raw taxa, expanded out to all levels present in any of the taxonomy strings (14 total levels).
  # - 'majority_taxonomy_7_levels'    # This file is the same as the 7 levels, but uses the 90% majority taxonomy.
  # - 'majority_taxonomy_all_levels'  # This file is the same as the all levels taxonomy, but uses the 90% majority taxonomy.
  # - 'consensus_taxonomy_7_levels'   # This file is the same as the 7 levels, but uses the 100% majority taxonomy.
  # - 'consensus_taxonomy_all_levels' # This file is the same as the all levels taxonomy, but uses the 100% majority taxonomy.

## ANCOM -----------------------------------------------------------------
ancom:
  level: # choose only one
    #- 0 # Life
    #- 0 # Domain       Bacteria / Eukarya
    #- 1 # Kingdom      Bacteria / Fungui
    - 2 # Phylum   Acidobacteria / Ascomycota
    #- 3 # Class                 / 
    #- 4 # Order                 /
    #- 5 # Familly               /
    #- 6 # Genus                 /
    #- 7 # Species               /

## GNEISS ----------------------------------------------------------------
gneiss:
  gradient:
  formula: 'Acronym'
  heatmap:
    factor:
      - 'Acronym'
      #- 'Age'
    n_dimension: '10'
    method:
      - 'clr' # default
      - 'log'
    color_map:
      - 'seismic' # default = viridis
      # YlGn|cool|coolwarm|RdYlBu|gist_heat|Vega20|gist_stern|bwr|PRGn|GnBu|BuGn|YlGnBu|afmhot|Purples|copper|gist_rainbow|flag|cubehelix|gnuplot|gist_ncar|Greens|PuRd|summer|
      # Pastel1|Set2|hsv|Vega20c|BuPu|gray|ocean|CMRmap|PuBu|Pastel2|winter|magma|jet|PiYG|PuBuGn|plasma|Paired|BrBG|Set1|gist_earth|Oranges|autumn|PuOr|YlOrRd|terrain|Dark2|
      # nipy_spectral|Vega20b|Vega10|spring|RdBu|YlOrBr|Set3|RdYlGn|Accent|inferno|viridis|RdPu|Spectral|prism|rainbow|Reds|RdGy|Greys|gnuplot2|brg|Blues|pink|hot|bone|seismic|OrRd
  balance:
    level: # choose only one
    #- 0 # Life
    #- 0 # Domain       Bacteria / Eukarya
    #- 1 # Kingdom      Bacteria / Fungui
    - 2 # Phylum   Acidobacteria / Ascomycota
    #- 3 # Class                 / 
    #- 4 # Order                 /
    #- 5 # Familly               /
    #- 6 # Genus                 /
    #- 7 # Species               /
  
## FUNGUILD --------------------------------------------------------------

funguild:
  script: # python script 'Guilds' (version and directory)
    #- 'opt/Guilds_v1.0.py' 
    - 'opt/Guilds_v1.1.py'
  database:
    - 'fungi'     # Fungi (default)
    #- 'nematode' # Nematode

## R_STAT_GRAPH ----------------------------------------------------------

r:
  mirror:
    #- 'https://cran.stat.auckland.ac.nz/' # New-Zeland (Auckland University)
    #- 'https://cran.ms.unimelb.edu.au/'  # Australia (Melbourne University)
    - 'https://cran.biotools.fr/'        # France (Marseille IBDM)
  extension:
    - 'pdf'
    - 'png'
    #- 'svg'
  graph:
    order: '"MAO","MD","MPGd","MP","ZRA1","ZRA2","ZRA3","ZRB","ZRC"'
    names: '"OLM","CLM","TCM.Gd","TCM.Fs","A1","A2","A3","B","C"'
    color: '"darkred","darkorange","darkblue","darkgreen","darkorchid1","darkorchid2","darkorchid3","darkorchid","darkorchid4"'
    shape: '16, 16, 16, 16, 10, 12, 9, 15, 17'
      
  dissimethode: # dissimilarity index, partial match to :
    - 'bray'        # Bray-Curtis dissimilarity: Fraction of overabundant counts. -----> d[jk]= (sum abs(x[ij]-x[ik])/(sum (x[ij]+x[ik]))
    - 'jaccard'     # Jaccard similarity index: Fraction of unique features, regardless of abundance.
    #- 'manhattan'  # Manhattan -------------------------------------------------------> d[jk]= sum(abs(x[ij] - x[ik]))
    #- 'euclidean'  # Euclidean distance: Species-by-species distance matrix ----------> d[jk]= sqrt(sum (x[ij]-x[ik])^2)
    #- 'canberra'   # Canberra distance: Overabundance on a feature by feature basis --> d[jk]= (1/NZ) sum ((x[ij]-x[ik])/(x[ij]+x[ik]))
    #- 'binomial'   # Binomial index is derived from Binomial deviance under null hypothesis that the two compared communities are equal.
                    # n[i] = x[ij] + x[ik] --> d[jk] = sum(x[ij]*log(x[ij]/n[i]) + x[ik]*log(x[ik]/n[i]) - n[i]*log(1/2))/n[i]
    #- 'chao'       # Chao index tries to take into account the number of unseen species pairs, similarly as Chao's method.
                    # d[jk] = U_j U_k/(U_j + U_k - U_j U_k)  where U_j = C_j/N_j + (N_k - 1)/N_k times a_1/(2 a_2) times S_j/N_j
    #- 'kulczynski' # Kulczynski dissimilarity index: Describes the dissimilarity between two samples
                    # d[jk] = 1 - 0.5*((sum min(x[ij],x[ik])/(sum x[ij]) + (sum min(x[ij],x[ik])/(sum x[ik]))
    #- 'gower'      # Gower, M is the number of columns (excluding missing values)
                    # d[jk] = (1/M) sum (abs(x[ij]-x[ik]) / (max(x[i])-min(x[i]))
    #- 'morisita'   # Morisitad, lambda[j]= sum(x[ij]*(x[ij]-1))/sum(x[ij])*sum(x[ij]-1)
                    #  d[jk] = 1 - 2*sum(x[ij]*x[ik]) / ((lambda[j]+lambda[k]) * sum(x[ij])*sum(x[ik]))
    #- 'horn'       # Horn, like morisita, but lambda[j]= sum(x[ij]^2)/(sum(x[ij])^2)
    #- 'mountford'  # Mountford index is defined as M = 1/alpha where alpha is the parameter of Fisher's logseries,
                    # and assuming that the compared communities are samples from the same community.
    #- 'raup'       # Raup-Crick dissimilarity is a probabilistic index based on presence/absence data. It is defined as 1-prob(j),
                    # or based on the probability of observing at least j species in shared in compared communities.

    # NOTES:
    # Bray-Curtis and Jaccard indices are rank-order similar,
    # and some other indices become identical or rank-order similar after some standardizations,
    # especially with presence/absence transformation of equalizing site totals with decostand.

    # Jaccard index is metric, and probably should be preferred instead of the default Bray-Curtis which is semimetric.

    # Euclidean and Manhattan dissimilarities are not good in gradient separation without proper standardization,
    # but are still included for comparison and special needs.

    # Morisita index can be used with genuine count data (integers) only.
    # Its Horn-Morisita variant is able to handle any abundance data.

## R_inext ---------------------------------------------------------------

inext:
  script: "opt/inext.R"
  q:                    # Number or vector specifying the diversity order(s) of Hill numbers.
    - '0'                 # Species richness (q=0)
    #- '1                  # Shannon diversity (q=1, the exponential of Shannon entropy)
    #- '2'                 # Simpson diversity (q=2, the inverse of Simpson concentration)
  data_type:            # Type of input data, 'abundance', 'incidence_raw' or 'incidence_freq'.
    - 'abundance'
    #- 'incidence_raw'
    #- 'incidence_freq'
  sample_size: 'NULL'   # Integer vector of sample sizes for which diversity estimates will be computed.  
                          # If NULL, then diversity estimates will be calculated for those sample sizes determined by the specified/default endpoint and knots.
  endpoint: 'NULL'       # Integer specifying the sample size that is the endpoint for R/E calculation.
                          # If NULL, then endpoint = double the reference sample size.
  knots: '20'            # Integer specifying the number of equally-spaced knots (default: '40') between size 1 and the endpoint.
  se: 'TRUE'             # Logical variable to calculate the bootstrap standard error and confidence interval of a level specified by conf.
  confidence: '0.95'     # Positive number < 1 specifying the level of confidence interval.
  n_boot: '10'          # Integer specifying the number of bootstrap replications.
  ggplot:
    facet:
      - 'none' 
      #- 'site'   # Creates a separate plot for each site.
      #- 'order'  # Creates a separate plot for each diversity order (0, 1 and 2).
      #- 'both'   # Creates a separate plot for each combination of diversity order and site,
    color:
      #- 'none'   #
      - 'site'    # Different colors used for the sites.
      #- 'order'  # Different colors used for the three orders.
      #- 'both'
    grey: 'FALSE' # If 'TRUE', Outputblack-and-white plots.
    point_size: 0.01

## R_Jaunatre ---------------------------------------------------------------

jaunatre:
  indices:
    assess:    # Tested plots
      #- 'MAO'
      #- 'MD'
      #- 'MPGd'
      #- 'MP'
      - 'ZR'
    reference: # References plots
      - 'MAO'
      - 'MD'
      - 'MPGd'
      - 'MP'
      - 'ZR'
    rar: # Minimum number of samples in which species have be present to be taken into account in the calculation of indices.
# It should not be used in the indices calculation,
# but it can be useful to reduce the number of species with the structure.plot() function.
      - 1  # For ITS2 and V4, all ASVs kept for indices %
      - 6  # For V4, around 30 ASVs kept
      - 12 # For ITS2, around 34 ASVs kept
  plotV2:  
    factor: # A factor list, a barplot of species mean abundances will be performed for each fact lvl.
      ITS2: '"A1", "A1", "A1", "A1", "A2", "A2", "A2", "A2", "A2", "A2", "A2", "A2", "A3", "A3", "A3", "B", "B", "B", "B", "C", "C", "C", "C", "C", "C", "C", "C"'
      V4: '"A1", "A1", "A1", "A1", "A2", "A2", "A2", "A2", "A2", "A2", "A2", "A3", "A3", "A3", "A3", "B", "B", "B", "B", "C", "C", "C", "C", "C", "C", "C", "C"'
    multi: 'T'         # If no factor is specified, OR, if not enough 'y' observationsMULTI = F should be specified.
    maxabundance: 500  # Numerical value of the maximum abundance.
    colref:            # Colour information for the Reference mean abundances barplot.
      DEFAULT: 'black'
      MAO: 'indianred4'
      MD: 'sienna4'
      MPGd: 'darkslategray4'
      MP: 'darkseagreen4'
      ZR: 'darkorchid4'
    colmiss:           # Colour information in assesssed community barplot, i.e. "missing abundances".
      DEFAULT: 'white'
      MAO: 'white'     #'indianred1'
      MD: 'white'      #'sienna1'
      MPGd: 'white'    #'darkslategray1'
      MP: 'white'       #'darkseagreen1'
      ZR: 'white'      #'darkorchid1'
    coltest:           # Colour information for the abundances of target species in the assesssed community.
      DEFAULT: 'black'
      MAO: 'indianred4'
      MD: 'sienna4'
      MPGd: 'darkslategray4'
      MP: 'darkseagreen4'
      ZR: 'darkorchid4'
    colhigher:         # Colour information for the "higher abundances" in the assessed community.
      DEFAULT: 'grey'
      MAO: 'grey'      #'indianred3'
      MD: 'grey'       #'sienna3'
      MPGd: 'grey'     #'darkslategray3'
      MP: 'grey'       #'darkseagreen3'
      ZR: 'grey'       #'darkorchid3'
    names: 'T'         # If other than "T", species names are not given.
    namesexp: 0.1      # Expansion factor for species names. Not used, expname = rar / 10.
    error:             # Error calculation (facultative). Beware, you can't use the 'na.rm=T' argument.
      - 'sem'           # "sem", standard error of the mean (default) 
      #- 'IC'            # "IC", confidence interval at 5 percent.
      #- 'var'           # "var".
      #- 'sd'            # "sd".
      #- ''              # or any other R function can be use.
    adjmeth:           # p adjust method. "bond","BH","hoch" or "none" (p: wilcoxon-t between ref. and mod.).
     - 'BH'             # "BH", Benjamini-Hochberg correction.
     #- 'bond'           # "bond", Bonferroni correction?
     #- 'hoch'           # "hoch", Hochberg correction.
     #- 'none'           # "none", no correction.
     #- 'holm'           # "holm", Holm-Bonferroni (not implemented yet)
    errorbarwidth: 0.08 # Error bar width.
    stars: 'T'         # Draw or note the stars, anything but "T" does not draw the stars. Default = "T".
    spstar: 1.8        # Width between stars and error bars.
    base: 'T'          # Draw or not the reference community for each modality. Default = T.
    #...               # Any arguments from a 'barplot' function can be used.

# Details: Be careful, the p adjustment is done only within a modality.
# Perhaps that would be better to adjust it through all the n modalitties (to be modified later).
# The warnings come from ties, but it seems it does not change the outcome of tests...

...