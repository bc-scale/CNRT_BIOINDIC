###############################################################################
# Author: N. Fernandez
# Affiliation: IAC
# Aim: convert BIOM format to table format
# Date: 2017.11.02
# Run: snakemake -j [core] --use-conda
# Latest modification: 2018.11.20
# Todo:

###############################################################################
configfile: "config.yaml"

###############################################################################
# Wildcards
PROJECT = config["datasets"]["project"]
PRIMERS = config["datasets"]["primers"]

BIOM_DIR, = glob_wildcards("out/"+PROJECT+"/"+PRIMERS[0]+"/qiime2/export/"
                          +"core/{biom_dir}/feature-table.biom")

SUBBIOM_DIR, = glob_wildcards("out/"+PROJECT+"/"+PRIMERS[0]+"/qiime2/export/"
                             +"subtables/{subbiom_dir}/feature-table.biom")

# Environment
QIIME2 = "../env/qiime2-2018.08.yaml"

################################################################################
rule all:
    input:    
        asv_table = expand("out/{project}/{primers}/qiime2/export/"
                           +"core/{biom_dir}/ASV.tsv",
                           project = PROJECT,
                           primers = PRIMERS,
                           biom_dir = BIOM_DIR),

        asv_subtable = expand("out/{project}/{primers}/qiime2/export/"
                              +"subtables/{subbiom_dir}/ASV.tsv",
                              project = PROJECT,
                              primers = PRIMERS,
                              subbiom_dir = SUBBIOM_DIR)

################################################################################
rule biom_format:
    # Aim: Remove first line and rename '#OTU ID' into 'ASV'
    # Use: sed [OPTIONS]
    input:
        tsv_file = ("out/{project}/{primers}/qiime2/export/core/{biom_dir}/"
                     +"table-from-biom.tsv")
    output:
        asv_table = ("out/{project}/{primers}/qiime2/export/core/{biom_dir}/"
                     +"ASV.tsv")
    shell:
        "sed '1d ; s/\#OTU ID/ASV_ID/' {input.tsv_file} > {output.asv_table}"
        
################################################################################
rule biom_convert:
    # Aim: Convert to/from the BIOM table format
    # Use: biom convert [OPTIONS]
    conda:
        QIIME2
    input:
        biom_file = ("out/{project}/{primers}/qiime2/export/core/{biom_dir}/"
                     +"feature-table.biom")
    output:
        tsv_file = ("out/{project}/{primers}/qiime2/export/core/{biom_dir}/"
                     +"table-from-biom.tsv")
    shell:
        "biom convert "
        "-i {input.biom_file} "
        "-o {output.tsv_file} "
        "--to-tsv"

################################################################################
rule subbiom_format:
    # Aim: Remove first line and rename '#OTU ID' into 'ASV'
    # Use: sed [OPTIONS]
    input:
        tsv_file = ("out/{project}/{primers}/qiime2/export/subtables/"
                     +"{subbiom_dir}/table-from-biom.tsv")
    output:
        asv_table = ("out/{project}/{primers}/qiime2/export/subtables/"
                     +"{subbiom_dir}/ASV.tsv")
    shell:
        "sed '1d ; s/\#OTU ID/ASV_ID/' {input.tsv_file} > {output.asv_table}"

################################################################################
rule subbiom_convert:
    # Aim: Convert to/from the BIOM table format
    # USe: biom convert [OPTIONS]
    conda:
        QIIME2
    input:
        biom_file = ("out/{project}/{primers}/qiime2/export/subtables/{subbiom_dir}/"
                     +"feature-table.biom")
    output:
        tsv_file = ("out/{project}/{primers}/qiime2/export/subtables/"
                     +"{subbiom_dir}/table-from-biom.tsv")
    shell:
        "biom convert "
        "-i {input.biom_file} "
        "-o {output.tsv_file} "
        "--to-tsv"

################################################################################
