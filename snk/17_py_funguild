###############################################################################
# Author: N. Fernandez
# Affiliation: IAC
# Aim: parse fungal community by ecological guild with FUNGuild annotation tool
# Date: 2017.11.02
# Run: snakemake -j [core] --use-conda
# Latest modification: 2018.11.20
# Todo:

###############################################################################
configfile: "config.yaml"

###############################################################################
# Wildcards
PROJECT = config["datasets"]["project"]

# Params
VERSION = config["funguild"]["script"]   # funguild
DATABASE = config["funguild"]["database"] # funguild

###############################################################################
rule all:
    input: 
        guilds = expand("out/{project}/its2/funguild/TaxoASV.guilds.txt",
                        project = PROJECT)
 
##############################################################################
rule funguild:
    # Aim: Parse fungal community datas by ecological guild with FUNGuild
    # Use: python Guilds_v1.0.py [OPTIONS]
    log:
        "out/{project}/its2/funguild/TaxoBiom.log"
    params:
        version = VERSION,
        database = DATABASE
    input:
        taxonomic_asv = "out/{project}/its2/funguild/TaxoASV.tsv"
    output:
        guilds = "out/{project}/its2/funguild/TaxoASV.guilds.txt"
    shell:
        "python3.5 {params.version} "
        # default output folder for table with all ASVs (.guilds)
        "-otu {input.taxonomic_asv} "
        "-db {params.database} "
        # table with only ASVs for which functional assignments could be made
         # (.guilds_matched)
        #"-m "
        # table with only ASVs that could not be assigned (.guilds_unmatched)
        #"-u "
        "1> {log}"

###############################################################################
rule taxonomy_asv:
    # Aim: Merge sorted biom and  taxonomy tsv tables
    # Use: paste [OPTIONS]
    input:
        sorted_asv = "out/{project}/its2/funguild/SortASV.tsv",
        filtered_taxonomy = "out/{project}/its2/funguild/FiltTaxo.tsv"
    output:
        taxonomic_asv = "out/{project}/its2/funguild/TaxoASV.tsv"
    shell:
        "paste {input.sorted_asv} {input.filtered_taxonomy} "
        "> {output.taxonomic_asv}"

###############################################################################
rule filter_taxonomy:
    # Aim: Filter taxonomy tsv table
    # Use: awk [OPTIONS] + sort [OPTIONS]
    input:
        sorted_taxonomy = "out/{project}/its2/funguild/SortTaxo.tsv"
    output:
        filtered_taxonomy = "out/{project}/its2/funguild/FiltTaxo.tsv"
    shell:
        "awk '{{print $2}}' {input.sorted_taxonomy} "
        "> {output.filtered_taxonomy}"

###############################################################################
rule sort_taxonomy:
    # Aim: Sort taxonomy tsv table by OTUs
    # Use: sed [OPTIONS] + sort [OPTIONS]
    input:
        taxonomy = ("out/{project}/its2/qiime2/export/taxonomy/Taxonomy/"
                    +"taxonomy.tsv")
    output:
        sorted_taxonomy = "out/{project}/its2/funguild/SortTaxo.tsv"
    shell:
        "sed 's/Taxon/taxonomy/ ; s/\Feature ID/ID/' {input.taxonomy} | "
        "sort -r > {output.sorted_taxonomy}"

###############################################################################
rule sort_asv:
    # Aim: Sort biom tsv table by ASVs
    # Use: sed [OPTIONS] + sort [OPTIONS]
    input:
        tsv_table = ("out/{project}/its2/qiime2/export/core/RarTable/"
                     +"table-from-biom.tsv")
    output:
        sorted_asv = "out/{project}/its2/funguild/SortASV.tsv"
    shell:
        "sed '1d ; s/\#OTU ID/ID/' {input.tsv_table} | "
        "sort -r > {output.sorted_asv}"

###############################################################################
