###############################################################################
# Author: N. Fernandez
# Affiliation: IAC
# Aim: gunzip fastq.gz files
# Date: 2017.11.02
# Run: snakemake -q -j [core] -s snk/00_gunzip -C [config]
# Latest modification: 2018.11.07
# Todo:

###############################################################################
configfile: "config.yaml"

###############################################################################
# Wildcards
PROJECT = config["datasets"]["project"]
PRIMERS = config["datasets"]["primers"]
SAMPLES, = glob_wildcards("inp/reads/"+PROJECT+"/"+PRIMERS[0]
                          +"/{samples}.fastq.gz")

###############################################################################
rule all:
    input:
        fastq = expand("out/{project}/{primers}/reads/raw/{samples}.fastq",
                       project = PROJECT,
                       primers = PRIMERS,
                       samples = SAMPLES)

###############################################################################
rule gunzip:
    # Aim: gunzip sample.fastq.gz into sample.fastq
    # Use: gunzip [OPTIONS] <FILEs>
    message:
        "Gunzip {wildcards.project} {wildcards.primers} {wildcards.samples}"
    input:
        gz_file = "inp/reads/{project}/{primers}/{samples}.fastq.gz"
    output:
        fastq = "out/{project}/{primers}/reads/raw/{samples}.fastq"
    shell:
        "gunzip "           # Gunzip uncompress FILEs (by default, in-place)
        "--quiet "          # -q: Suppress all warnings
        "--keep "           # -k: Keep (don't delete) input files
        "--stdout "         # -c: Write on standard output, keep original files
        "{input.gz_file} "  # input
        "> {output.fastq}"  # Add redirection to output

###############################################################################
