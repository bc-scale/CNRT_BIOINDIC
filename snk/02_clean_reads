###############################################################################
# Author: N. Fernandez
# Affiliation: IAC
# Aim: clean reads
# Date: 2017.11.02
# Run: snakemake -q -j [core] -s snk/02_clean -C [config] --use-conda
# Latest modification: 2018.11.07
# Todo:

###############################################################################
configfile: "config.yaml"

###############################################################################
# Wildcards
PROJECT = config["datasets"]["project"]
PRIMERS = config["datasets"]["primers"]
SAMPLES, = glob_wildcards("out/"+PROJECT+"/"+PRIMERS[0]+"/reads/raw/"
                          +"{samples}_R1.fastq")

# Params
COMMANDE = config["sickle"]["commande"] # sickle
ENCODING = config["sickle"]["encoding"] # sickle
QUALITY = config["sickle"]["quality"]   # sickle
LENGTH = config["sickle"]["length"]     # sickle

TRUSEQ = config["cutadapt"]["truseq"]   # cutadapt
NEXTERA = config["cutadapt"]["nextera"] # cutadapt
SMALL = config["cutadapt"]["small"]     # cutadapt

###############################################################################
rule all:
    input:
        joined = expand("out/{project}/{primers}/reads/joined/{samples}.fastq",
                        project = PROJECT,
                        primers = PRIMERS,
                        samples = SAMPLES)

###############################################################################
rule fastqjoin:
    # Aim: joins two paired-end reads on the overlapping ends
    # Use: fastq-join [options] <read1.fastq> <read2.fastq> [mate.fastq]
     # -o <read.fastq> -o <read.fastq> -o <read.fastq>
    message:
        "fastqjoin {wildcards.project} {wildcards.primers} {wildcards.samples}"
    conda:
        "../env/quality.yaml"
    log:
        "out/{project}/{primers}/log/fastqjoin/{samples}.log"
    input:
        forward = "out/{project}/{primers}/reads/sickle/{samples}_R1.fastq",
        reverse = "out/{project}/{primers}/reads/sickle/{samples}_R2.fastq"
    output:
        forward= "out/{project}/{primers}/reads/uniq/{samples}_R1.fastq",
        reverse = "out/{project}/{primers}/reads/uniq/{samples}_R2.fastq",
        joined = "out/{project}/{primers}/reads/joined/{samples}.fastq"
    shell:
        "fastq-join "          # Fastq-Join
        "{input.forward} "     # Input forward
        "{input.reverse} "     # Input reverse
        "-o {output.forward} " # for unique forward files
        "-o {output.reverse} " # for unique reverse files
        "-o {output.joined} "  # for join files
        "1> {log}"             # Add redirection for log

###############################################################################
rule sickle:
    # Aim: windowed adaptive trimming tool for FASTQ files using quality
    # Use: sickle <command> [options]
    message:
        "sickle {wildcards.project} {wildcards.primers} {wildcards.samples}"
    conda:
        "../env/quality.yaml"
    log:
        "out/{project}/{primers}/log/sickle/{samples}.log"
    params:
        commande = config["sickle"]["commande"],
        encoding = config["sickle"]["encoding"],
        quality = config["sickle"]["quality"],
        length = config["sickle"]["length"]
    input:
        forward = "out/{project}/{primers}/reads/cutadapt/{samples}_R1.fastq",
        reverse = "out/{project}/{primers}/reads/cutadapt/{samples}_R2.fastq"
    output:
        forward = "out/{project}/{primers}/reads/sickle/{samples}_R1.fastq",
        reverse = "out/{project}/{primers}/reads/sickle/{samples}_R2.fastq",
        single = "out/{project}/{primers}/reads/sickle/{samples}_Single.fastq"
    shell:
        # Sickle
        "sickle "
        # Paired-end or single-end usage
        "{params.commande} "
        # --qual-type: Type of quality values, solexa ; illumina ; sanger
         # CASAVA, < 1.3 ; 1.3 to 1.7 ; >= 1.8
        "-t {params.encoding} "
        # --qual-threshold: Threshold for trimming based on average quality
         # in a window (default 20)
        "-q {params.quality} "
        # --length-threshold: Threshold to keep a read based on length
        # after trimming (default 20)
        "-l {params.length} "
        # --pe-file1: Input paired-end forward fastq file
        "-f {input.forward} "
        # --pe-file2: Input paired-end reverse fastq file
        "-r {input.reverse} "
        # --output-pe1: Output trimmed forward fastq file
        "-o {output.forward} "
        # --output-pe2: Output trimmed reverse fastq file
        "-p {output.reverse} "
        # --output-single: Paired-end interleaved reads
        "-s {output.single} "
        # Add redirection for log
        "1> {log}"

###############################################################################
rule cutadapt:
    # Aim: removes adapter sequences from high-throughput sequencing reads
    # Use: cutadapt -a ADAPTER [options] [-o output.forward] [-p output.reverse]
     # <input.forward> <input.reverse>
    # Rmq: multiple adapter sequences can be given using further -a options,
         # but only the best-matching adapter will be removed
    message:
        "cutadapt {wildcards.project} {wildcards.primers} {wildcards.samples}"
    conda:
        "../env/quality.yaml"
    log:
        "out/{project}/{primers}/log/cutadapt/{samples}.log"
    params:
        truseq = config["cutadapt"]["truseq"],
        nextera = config["cutadapt"]["nextera"],
        small = config["cutadapt"]["small"]
    input:
        forward = "out/{project}/{primers}/reads/raw/{samples}_R1.fastq",
        reverse = "out/{project}/{primers}/reads/raw/{samples}_R2.fastq"
    output:
        forward = "out/{project}/{primers}/reads/cutadapt/{samples}_R1.fastq",
        reverse = "out/{project}/{primers}/reads/cutadapt/{samples}_R2.fastq"
    shell:
        # Cutadapt
        "cutadapt "
        # -a: Sequence of an adapter ligated to the 3' end of the first read
        "--adapter {params.truseq} "
        # -A: 3' adapter to be removed from second read in a pair
        "-A {params.truseq} "
        # -a: Sequence of an adapter ligated to the 3' end of the first read
        "--adapter {params.nextera} "
        # -A: 3' adapter to be removed from second read in a pair
        "-A {params.nextera} "
        # -a: Sequence of an adapter ligated to the 3' end of the first read
        "--adapter {params.small} "
        # -A: 3' adapter to be removed from second read in a pair
        "-A {params.small} "
        # -o: Write trimmed reads to FILE
        "--output {output.forward} "
        # -p: Write second read in a pair to FILE
        "--paired-output {output.reverse} "
        # Input forward
        "{input.forward} "
        # Input reverse
        "{input.reverse} "
        # Add redirection for log
        "1> {log}"

###############################################################################
