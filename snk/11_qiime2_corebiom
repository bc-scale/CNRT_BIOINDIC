###############################################################################
# Author: N. Fernandez
# Affiliation: IAC
# Aim: identify "core" features observed in a fraction of the samples
# Date: 2017.11.02
# Run: snakemake -j [core] --use-conda
# Latest modification: 2018.11.16
# Todo:

###############################################################################
configfile: "config.yaml"

###############################################################################
# Wildcards
PROJECT = config["datasets"]["project"]
PRIMERS = config["datasets"]["primers"]
FORMATION = config["corebiom"]["formation"]

# Environment
QIIME2 = config["conda"]["qiime2"]

# Params
METADATA = config["datasets"]["metadata"] # split

MIN_FRACTION = config["corebiom"]["min_fraction"] # corebiom
MAX_FRACTION = config["corebiom"]["max_fraction"] # corebiom
STEPS = config["corebiom"]["steps"]             # corebiom

###############################################################################
rule all:
    input:
       core_biom = expand("out/{project}/{primers}/qiime2/visual/"
                          +"CoreBiom-{formation}.qzv",
                          project = PROJECT,
                          primers = PRIMERS,
                          formation = FORMATION),

       core_biom_all = expand("out/{project}/{primers}/qiime2/visual/"
                              +"CoreBiomAll.qzv",
                              project = PROJECT,
                              primers = PRIMERS)

###############################################################################
rule corebiom_all:
    # Aim: Identify "core" features, which are features observed,
     # in a user-defined fraction of the samples
    # Use: qiime feature-table core-features [OPTIONS]
    conda:
        QIIME2
    params:
        min_fraction = MIN_FRACTION,
        max_fraction = MAX_FRACTION,
        steps = STEPS
    input:
        rarefied_table = "out/{project}/{primers}/qiime2/core/RarTable.qza"
    output:
        core_biom = "out/{project}/{primers}/qiime2/visual/CoreBiomAll.qzv"
    shell:
        "qiime feature-table core-features "
        "--i-table {input.rarefied_table} "
        "--p-min-fraction {params.min_fraction} "
        "--p-max-fraction {params.max_fraction} "
        "--p-steps {params.steps} "
        "--o-visualization {output.core_biom}"

###############################################################################
rule corebiom_formation:
    # Aim: Identify "core" features, which are features observed,
     # in a user-defined fraction of the samples
    # Use: qiime feature-table core-features [OPTIONS]
    conda:
        QIIME2
    params:
        min_fraction = MIN_FRACTION,
        max_fraction = MAX_FRACTION,
        steps = STEPS
    input:
       sub_table = ("out/{project}/{primers}/qiime2/subtables/"
                    +"RarTable-{formation}.qza")
    output:
       core_biom = ("out/{project}/{primers}/qiime2/visual/"
                    +"CoreBiom-{formation}.qzv")
    shell:
        "qiime feature-table core-features "
        "--i-table {input.sub_table} "
        "--p-min-fraction {params.min_fraction} "
        "--p-max-fraction {params.max_fraction} "
        "--p-steps {params.steps} "
        "--o-visualization {output.core_biom}"

###############################################################################
rule split_table:
    # Aim: Filter sample from table based on a feature table or metadata
    # Use: qiime feature-table filter-samples [OPTIONS]
    conda:
        QIIME2
    params:
        metadata = METADATA
    input:
        rarefied_table = "out/{project}/{primers}/qiime2/core/RarTable.qza"
    output:
        sub_table = ("out/{project}/{primers}/qiime2/subtables/"
                     +"RarTable-{formation}.qza")
    shell:
        "qiime feature-table filter-samples "
        "--i-table {input.rarefied_table} "
        "--m-metadata-file {params.metadata} "
        "--p-where 'Acronyme = \"{wildcards.formation}\"' "
        "--o-filtered-table {output.sub_table}"

###############################################################################
